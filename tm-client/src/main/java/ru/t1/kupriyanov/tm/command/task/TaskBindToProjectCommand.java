package ru.t1.kupriyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.TaskBindToProjectRequest;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(projectId, taskId);
        getTaskEndpoint().bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Bind task to project";
    }

}
