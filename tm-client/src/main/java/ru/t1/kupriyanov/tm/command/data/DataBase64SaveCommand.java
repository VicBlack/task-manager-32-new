package ru.t1.kupriyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.DataBase64SaveRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        getDomainEndpoint().saveDataBase64(new DataBase64SaveRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return "save-base64-data";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data to a base64 file.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
