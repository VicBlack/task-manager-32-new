package ru.t1.kupriyanov.tm.api.service;

public interface IDomainService {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXml();

    void loadDataJsonJaxB();

    void saveDataJsonFasterXml();

    void saveDataJsonJaxB();

    void loadDataXmlFasterXml();

    void loadDataXmlJaxB();

    void saveDataXmlFasterXml();

    void saveDataXmlJaxB();

    void loadDataYamlFasterXml();

    void saveDataYamlFasterXml();

}
