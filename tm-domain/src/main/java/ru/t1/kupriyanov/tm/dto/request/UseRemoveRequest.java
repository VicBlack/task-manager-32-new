package ru.t1.kupriyanov.tm.dto.request;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UseRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UseRemoveRequest(@Nullable String login) {
        this.login = login;
    }

}
