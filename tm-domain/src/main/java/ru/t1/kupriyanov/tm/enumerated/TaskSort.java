package ru.t1.kupriyanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.comparator.CreatedComparator;
import ru.t1.kupriyanov.tm.comparator.NameComparator;
import ru.t1.kupriyanov.tm.comparator.StatusComparator;
import ru.t1.kupriyanov.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<Task> comparator;

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final TaskSort taskSort : values()) {
            if (taskSort.name().equals(value)) return taskSort;
        }
        return null;
    }

    TaskSort(@NotNull String displayName, @NotNull Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @SuppressWarnings("rawtypes")
    @NotNull
    public Comparator<Task> getComparator() {
        return comparator;
    }

}
