package ru.t1.kupriyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.DataBinarySaveRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return "save-binary-data";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data to a binary file.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}