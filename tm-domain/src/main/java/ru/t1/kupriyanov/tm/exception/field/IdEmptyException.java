package ru.t1.kupriyanov.tm.exception.field;

public final class IdEmptyException extends AbstractFiledException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
